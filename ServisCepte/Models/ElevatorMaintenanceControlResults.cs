﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorMaintenanceControlResults
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public int? ElevatorOrder { get; set; }
        public int? EmcdId { get; set; }
        public int? EmcdResult { get; set; }
        public DateTime? DateTime { get; set; }
        public string Token { get; set; }
        public int? UserId { get; set; }
        public sbyte? Stats { get; set; }
        public int? StatsUser { get; set; }
        public sbyte? TypeNum { get; set; }

        public Customer Customer { get; set; }
    }
}
