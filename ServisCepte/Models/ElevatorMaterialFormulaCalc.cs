﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorMaterialFormulaCalc
    {
        public int Id { get; set; }
        public int? ElevatorMaterialFormulaId { get; set; }
        public string QtyFormula { get; set; }
        public string SizeFormula1 { get; set; }
        public string SizeFormula2 { get; set; }
        public string SizeFormula3 { get; set; }

        public ElevatorMaterialFormula ElevatorMaterialFormula { get; set; }
    }
}
