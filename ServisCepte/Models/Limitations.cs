﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class Limitations
    {
        public int Id { get; set; }

        public LimitationsModules LimitationsModules { get; set; }
    }
}
