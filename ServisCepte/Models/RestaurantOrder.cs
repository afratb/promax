﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class RestaurantOrder
    {
        public int Id { get; set; }
        public int? TableId { get; set; }
        public int? Basket { get; set; }
        public string CountOfCustomer { get; set; }
        public int? TableNumber { get; set; }
        public DateTime? InsDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public decimal? DiscountRate { get; set; }
        public decimal? TotalAmount { get; set; }
        public int? UserId { get; set; }
        public sbyte? Deletion { get; set; }
        public int? BillUserId { get; set; }
        public sbyte? PayType { get; set; }
    }
}
