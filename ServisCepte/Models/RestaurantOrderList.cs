﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class RestaurantOrderList
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal? Qty { get; set; }
        public int? RestaurantOrderBasketId { get; set; }
        public sbyte? Status { get; set; }
        public decimal? Portion { get; set; }
        public int? Group3Id { get; set; }
        public int? Group2Id { get; set; }
        public decimal? Price { get; set; }
        public decimal? Amount { get; set; }
        public int? UserId { get; set; }
    }
}
