﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class RestaurantProductFlavorDetails
    {
        public RestaurantProductFlavorDetails()
        {
            RestaurantOrderListTemp = new HashSet<RestaurantOrderListTemp>();
        }

        public int Id { get; set; }
        public int? ProductId { get; set; }
        public string FlavorDefine { get; set; }
        public decimal? Price { get; set; }

        public Product Product { get; set; }
        public ICollection<RestaurantOrderListTemp> RestaurantOrderListTemp { get; set; }
    }
}
