﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorMobileMaintenance
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public int? UserId { get; set; }
        public DateTime? InsTime { get; set; }
        public DateTime? MaintenanceDatetime { get; set; }
        public string Notes { get; set; }
        public decimal? AmountOfCollection { get; set; }
        public sbyte? Stats { get; set; }
        public int? CustomerCurrentOutputId { get; set; }
    }
}
