﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class RestaurantDiscountRates
    {
        public int Id { get; set; }
        public string Exp { get; set; }
        public decimal? Rate { get; set; }
        public int? UserId { get; set; }
    }
}
