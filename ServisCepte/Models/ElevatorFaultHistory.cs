﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorFaultHistory
    {
        public int Id { get; set; }
        public int? ElevatorFaultId { get; set; }
        public DateTime? InsDateTime { get; set; }
        public sbyte? Status { get; set; }
        public string Exp1 { get; set; }
        public string Exp2 { get; set; }
        public int? UserId { get; set; }

        public ElevatorFault ElevatorFault { get; set; }
    }
}
