﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ProducerReceiptTaxDefines
    {
        public ProducerReceiptTaxDefines()
        {
            InvoiceProducerReceiptTaxList = new HashSet<InvoiceProducerReceiptTaxList>();
        }

        public int Id { get; set; }
        public string TaxCode { get; set; }
        public string TaxName { get; set; }
        public string TaxRate { get; set; }
        public string Exp { get; set; }
        public string Exp2 { get; set; }
        public string Exp3 { get; set; }

        public ICollection<InvoiceProducerReceiptTaxList> InvoiceProducerReceiptTaxList { get; set; }
    }
}
