﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class InvoiceProducerReceiptTaxList
    {
        public int Id { get; set; }
        public int? InvoiceId { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Tax { get; set; }
        public int? CustomerId { get; set; }
        public int? ProducerReceiptTaxDefinesId { get; set; }
        public string TaxName { get; set; }
        public DateTime? InsDatetime { get; set; }

        public Customer Customer { get; set; }
        public Invoice Invoice { get; set; }
        public ProducerReceiptTaxDefines ProducerReceiptTaxDefines { get; set; }
    }
}
