﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorFaultDefines
    {
        public ElevatorFaultDefines()
        {
            ElevatorFault = new HashSet<ElevatorFault>();
        }

        public int Id { get; set; }
        public string FaultDefine { get; set; }
        public string Exp { get; set; }
        public int? ResponsibleCustomerId { get; set; }
        public string ResponsibleAccountTitle { get; set; }

        public ICollection<ElevatorFault> ElevatorFault { get; set; }
    }
}
