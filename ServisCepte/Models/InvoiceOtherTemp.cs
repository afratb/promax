﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class InvoiceOtherTemp
    {
        public int Id { get; set; }
        public string Exp { get; set; }
        public decimal? Amount { get; set; }
        public int? TaxRate { get; set; }
        public string TaxAmount { get; set; }
        public int? Basket { get; set; }
    }
}
