﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorMaintenanceControlNotes
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public int? CustomerId { get; set; }
        public string Notes { get; set; }
        public DateTime? InsDateTime { get; set; }
    }
}
