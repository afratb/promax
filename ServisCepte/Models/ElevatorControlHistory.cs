﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorControlHistory
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? ContralDate { get; set; }
        public DateTime? NextControlDate { get; set; }
        public int? ControllerCompanyId { get; set; }
        public string ControllerCompanyName { get; set; }
        public string ControllerCompanyPersonelName { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }
        public string Exp { get; set; }
        public sbyte? ControlResult { get; set; }
        public sbyte? NeedRevision { get; set; }
        public sbyte? NeedSecondControl { get; set; }
        public sbyte? Deletion { get; set; }
        public int? RevisionOfferId { get; set; }
        public string ControllerMobilePhone { get; set; }

        public Customer Customer { get; set; }
    }
}
