﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorMaintenanceControlDefines
    {
        public ElevatorMaintenanceControlDefines()
        {
            ElevatorMaintenanceControlSubDefines = new HashSet<ElevatorMaintenanceControlSubDefines>();
        }

        public int Id { get; set; }
        public sbyte? ControlType { get; set; }
        public int? OrderNumber { get; set; }
        public sbyte? IsImportant { get; set; }
        public string ControlDefine { get; set; }
        public sbyte Active { get; set; }
        public string Code { get; set; }
        public sbyte? Color { get; set; }

        public ICollection<ElevatorMaintenanceControlSubDefines> ElevatorMaintenanceControlSubDefines { get; set; }
    }
}
