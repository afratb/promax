﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServisCepte.Models.PocoModels
{
    public class CustomerModel
    {
        public int borcfarkgun { get; set; }
        public int alacakfarkgun { get; set; }
        public int farkgun { get; set; }
        public sbyte? visible { get; set; }
        public string account_grup { get; set; }
        public int? route_order { get; set; }
        public string sales_block { get; set; }
        public string account_type { get; set; }
        public string contact_person { get; set; }
        public string task { get; set; }
        public string customer_code { get; set; }
        public int Id { get; set; }
        public string account_title { get; set; }
        public string tax_office { get; set; }
        public string tax_number { get; set; }
        public string adress2 { get; set; }
        public string adress { get; set; }
        public sbyte? price_privileged { get; set; }
        public int? pricing_id { get; set; }
        public string mobile_phone { get; set; }
        public string total_output { get; set; }
        public string total_input { get; set; }
        public string balance { get; set; }
        public sbyte? stats { get; set; } //elevatormainsontrolresult

        public CustomerModel(Customer c)
        {
            this.visible = c.Visible;
            this.account_grup = c.AccountGrup;
            this.route_order = c.RouteOrder;
            this.sales_block = c.SalesBlock;
            this.account_type = c.AccountType;
            this.contact_person = c.ContactPerson;
            this.task = c.Task;
            this.customer_code = c.CustomerCode;
            this.Id = c.Id;
            this.account_title = c.AccountTitle;
            this.tax_office = c.TaxOffice;
            this.tax_number = c.TaxNumber;
            this.adress2 = c.Adress2;
            this.adress = c.Adress;
            this.price_privileged = c.PricePrivileged;
            this.pricing_id = c.PricingId;
            this.mobile_phone = c.MobilePhone;
            //this.stats = c.ElevatorMaintenanceControlResults.Where(k => k.CustomerId=this.Id);
        }
    }
}
