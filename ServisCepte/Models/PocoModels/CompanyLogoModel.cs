﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServisCepte.Models.PocoModels
{
    public class CompanyLogoModel
    {
        public string mp_top1 { get; set; }
        public string mp_top2 { get; set; }
        public string mp_bottom1 { get; set; }
        public string push_company_code { get; set; }
        public string company_name { get; set; }
        public string mp_bottom2 { get; set; }
        public byte[] servis_cepte_logo_base_64 { get; set; }
        
        public CompanyLogoModel(GeneralParameters gp)
        {
            this.mp_top1 = gp.MpTop1;
            this.mp_top2 = gp.MpTop2;
            this.mp_bottom1 = gp.MpBottom1;
            this.push_company_code = gp.PushCompanyCode;
            this.company_name = gp.CompanyName;
            this.mp_bottom2 = gp.MpBottom2;
            this.servis_cepte_logo_base_64 = gp.ServisCepteLogoBase64;

        }
    }
}
