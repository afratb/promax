﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServisCepte.Models.PocoModels
{
    public class ControlDefinesModel
    {
        public int Id { get; set; }
        public int? order_number { get; set; }
        public sbyte? is_important { get; set; }
        public string control_define { get; set; }
        public sbyte active { get; set; }
        public string code { get; set; }
        public sbyte? color { get; set; }
        public int? services_request_defination_id { get; set; }
        public int? services_category_sub_definition_id { get; set; }
        public sbyte? control_type { get; set; }
        public sbyte? force_select { get; set; }

        public ControlDefinesModel(ServiceControlDefines scd)
        {
            this.Id = scd.Id;
            this.order_number = scd.OrderNumber;
            this.is_important = scd.IsImportant;
            this.control_define = scd.ControlDefine;
            this.active = scd.Active;
            this.code = scd.Code;
            this.color = scd.Color;
            this.services_request_defination_id = scd.ServicesRequestDefinationId;
            this.services_category_sub_definition_id = scd.ServicesCategorySubDefinitionId;
            this.force_select = scd.ForceSelect;
        }
    }
}
