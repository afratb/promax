﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServisCepte.Models.PocoModels
{
    public class AccountActivitiesModel
    {
        public int farkgun { get; set; }
        public string doc_number { get; set; }
        public string alacaktutari { get; set; }
        public string borctutari { get; set; }
        public int Id { get; set; }
        public int? detail_id { get; set; }
        public DateTime? datetime { get; set; }
        public sbyte? type { get; set; }
        public decimal? currency { get; set; }
        public decimal? tutar { get; set; }
        public string exp { get; set; }
        public decimal? bakiye { get; set; } 
        public string stats { get; set; }
        public string activity_type { get; set; }

        public AccountActivitiesModel(CustomerCurrentInput cc)
        {
            this.doc_number = cc.DocNumber;
            this.Id = cc.Id;
            this.detail_id = cc.DetailId;
            this.datetime = cc.Datetime;
            this.type = cc.Type;
            this.currency = cc.Currency;
            this.exp = cc.Exp;
        }
    }
}
