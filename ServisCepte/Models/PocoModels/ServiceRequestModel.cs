﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ServisCepte.Models.PocoModels
{
    public class ServiceRequestModel
    {

        public int Id { get; set; }
        public string defination { get; set; }
        public string code { get; set; }
        public string color { get; set; }
        public string hex_color { get; set; }
        public string color_code { get; set; }
        public int? order { get; set; }
        public string exp { get; set; }
        public int? services_category_definition_id { get; set; }
        public string services_category_name { get; set; }
        public sbyte? period { get; set; }
        public sbyte? component_dialog { get; set; }
        public sbyte? mobile_visible { get; set; }
        public sbyte? open_without_personel { get; set; }
        public sbyte? Customizable_sub_module { get; set; }
        public sbyte? allow_tcept { get; set; }//AllowNotAccept 
        public int? sk_tt_timeout { get; set; }//TaskAcceptTimeout
        public sbyte? automatic_charging { get; set; }
        public sbyte? forced_oact { get; set; }//ForcedContract

        public ServiceRequestModel(ServicesRequestDefination srd)
        {
            this.Id = srd.Id;
            this.defination = srd.Defination;
            this.code = srd.Code;
            this.color = srd.Color;
            this.hex_color = srd.HexColor;
            this.color_code = srd.ColorCode;
            this.order = srd.Order;
            this.services_category_definition_id = srd.ServicesCategoryDefinitionId;
            this.services_category_name = srd.ServicesCategoryName;
            this.period = srd.Period;
            this.component_dialog = srd.ComponentDialog;
            this.mobile_visible = srd.MobileVisible;
            this.open_without_personel = srd.OpenWithoutPersonel;
            this.Customizable_sub_module = srd.CustomizableSubModule;
            this.allow_tcept = srd.AllowNotAccept;
            this.sk_tt_timeout = srd.TaskAcceptTimeout;
            this.automatic_charging = srd.AutomaticCharging;
            this.forced_oact = srd.ForcedContract;
        }
        
    }
}
