﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServisCepte.Models.PocoModels
{
    public class ServicePointModel
    {
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string customer_code { get; set; } //customer
        public string account_grup { get; set; } //c
        public string account_title { get; set; } //c
        public int sozlesmekalangun { get; set; }
        public DateTime? start_date { get; set; } //services contract
        public DateTime? end_date { get; set; } //sc
        public string color { get; set; } //services category def
        public int Id { get; set; }
        public DateTime? ttance_date { get; set; }
        public sbyte? manufacturer { get; set; }
        public sbyte? warranty { get; set; }
        public DateTime? due_date { get; set; }
        public DateTime? installation_date { get; set; }
        public int? customer_id { get; set; }
        public int? rrvice_order { get; set; }
        public string identification_number { get; set; }
        public string service_code { get; set; }
        public string service_name { get; set; }
        public string direction { get; set; }
        public int? services_category_definition_id { get; set; }
        public string services_category_name { get; set; }
        public sbyte? deletion { get; set; }
        public int? services_contract_id { get; set; }
        public sbyte? active { get; set; }

        public ServicePointModel(ServicesPointsBaseInformation sp,Customer c,ServicesContract sc, ServicesCategoryDefinition scd)
        {
            this.latitude = sp.Latitude;
            this.longitude = sp.Longitude;
            this.customer_code = c.CustomerCode;
            this.account_grup = c.AccountGrup;
            this.account_title = c.AccountTitle;
            this.start_date = sc.StartDate;
            this.end_date = sc.EndDate;
            this.color = scd.Color;
            this.Id = sp.Id;
            this.ttance_date = sp.AcceptanceDate;
            this.manufacturer = sp.Manufacturer;
            this.warranty = sp.Warranty;
            this.due_date = sp.DueDate;
            this.installation_date = sp.InstallationDate;
            this.customer_id = sp.CustomerId;
            this.rrvice_order = sp.ServiceOrder;
            this.identification_number = sp.IdentificationNumber;
            this.service_code = sp.ServiceCode;
            this.service_name = sp.ServiceName;
            this.direction = sp.Direction;
            this.services_category_definition_id = sp.ServicesCategoryDefinitionId;
            this.services_category_name = sp.ServicesCategoryName;
            this.deletion = sp.Deletion;
            this.services_contract_id = sp.ServicesContractId;
            this.active = sp.Active;


        }
    }
}
