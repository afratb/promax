﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServisCepte.Models.PocoModels
{
    public class ServiceListModel
    {
        /// <summary>
        /// /////////////////////////////////////////////
        /// </summary>
        public int? services_open_id { get; set; }
        public int? services_points_base_information_id { get; set; }
        public sbyte? FaultStats { get; set; }
    }
}
