﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServisCepte.Models.PocoModels
{
    public class ProductModel
    {
        public sbyte? Warranty_period { get; set; }
        public int? product_id { get; set; }  //mainproduct
        public string barcode { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string unit { get; set; }
        public decimal? retail_price_tax { get; set; }
        public string private_code { get; set; }

        public ProductModel(Product p)
        {
            this.Warranty_period = p.WarrantyPeriod;
            this.product_id = p.MainProductId;
            this.barcode = p.Barcode;
            this.code = p.Code;
            this.name = p.Name;
            this.unit = p.Unit;
            this.retail_price_tax = p.RetailPriceTax;
            this.private_code = p.PrivateCode;
        }
    }
}
