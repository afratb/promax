﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServisCepte.Models.PocoModels
{
    public class ModuleDefinesModel
    {
        public int Id { get; set; }
        public string defination { get; set; }
        public string code { get; set; }
        public string color { get; set; }
        public int? order { get; set; }
        public string exp { get; set; }
        public int? services_category_definition_id { get; set; }
        public sbyte? active { get; set; }
        public sbyte? force_qr_code { get; set; }
        public sbyte? service_time_out_minutes { get; set; }
        public sbyte? force_ask_piece_change { get; set; }

        public ModuleDefinesModel(ServicesCategorySubDefinition scsd)
        {
            this.Id = scsd.Id;
            this.defination = scsd.Defination;
            this.code = scsd.Code;
            this.color = scsd.Color;
            this.order = scsd.Order;
            this.exp = scsd.Exp;
            this.services_category_definition_id = scsd.ServicesCategoryDefinitionId;
            this.active = scsd.Active;
            this.force_qr_code = scsd.ForceQrCode;
            this.service_time_out_minutes = scsd.ServiceTimeOutMinutes;
            this.force_ask_piece_change = scsd.ForceAskPieceChange;
        }
    }
}
