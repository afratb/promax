﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServisCepte.Models.PocoModels
{
    public class LastStatusModel
    {
        public int Id { get; set; }
        public string defination { get; set; }
        public string code { get; set; }
        public string color { get; set; }
        public int? order { get; set; }
        public string exp { get; set; }
        public int? ceces_request_defination_id { get; set; }
        public string sevices_request_defination_defination { get; set; }
        public int? services_category_definition_id { get; set; }
        public string services_category_name { get; set; }
        public sbyte? open_new_service { get; set; }
        public sbyte? service_impact { get; set; }
        public string open_new_service_request_defination_id { get; set; }
        public string open_new_service_request_defination_defination { get; set; }
        public string color_code { get; set; }
        public sbyte? deletion { get; set; }
        public DateTime? deletion_datetime { get; set; }
        public int? deletion_user_id { get; set; }
        public int? sms_themes_id { get; set; }
        public string sms_themes_name { get; set; }

        public LastStatusModel(ServicesLastStatusDefination slsd)
        {
            this.Id = slsd.Id;
            this.defination = slsd.Defination;
            this.code = slsd.Code;
            this.color = slsd.Color;
            this.order = slsd.Order;
            this.exp = slsd.Exp;
            this.ceces_request_defination_id = slsd.SevicesRequestDefinationId;
            this.sevices_request_defination_defination = slsd.SevicesRequestDefinationDefination;
            this.services_category_definition_id = slsd.ServicesCategoryDefinitionId;
            this.services_category_name = slsd.ServicesCategoryName;
            this.open_new_service = slsd.OpenNewService;
            this.service_impact = slsd.ServiceImpact;
            this.open_new_service_request_defination_id = slsd.OpenNewServiceRequestDefinationId;
            this.open_new_service_request_defination_defination = slsd.OpenNewServiceRequestDefinationDefination;
            this.color_code = slsd.ColorCode;
            this.deletion = slsd.Deletion;
            this.deletion_datetime = slsd.DeletionDatetime;
            this.deletion_user_id = slsd.DeletionUserId;
            this.sms_themes_id = slsd.SmsThemesId;
            this.sms_themes_name = slsd.SmsThemesName;

        }
    }
}
