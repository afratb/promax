﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServisCepte.Models.PocoModels
{
    public class SubControlDefinesModel
    {
        public int Id { get; set; }
        public int? order_number { get; set; }
        public sbyte? is_important { get; set; }
        public string control_define { get; set; }
        public sbyte active { get; set; }
        public int? service_control_defines_id { get; set; }
        public sbyte? force { get; set; }
        public sbyte? control_type { get; set; }

        public SubControlDefinesModel(ServiceControlSubDefines scsd)
        {
            this.Id = scsd.Id;
            this.order_number = scsd.OrderNumber;
            this.is_important = scsd.IsImportant;
            this.control_define = scsd.ControlDefine;
            this.service_control_defines_id = scsd.ServiceControlDefinesId;
            this.force = scsd.Force;
        }
    }
}
