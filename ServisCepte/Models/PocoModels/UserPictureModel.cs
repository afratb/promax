﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServisCepte.Models.PocoModels
{
    public class UserPictureModel
    {
        public byte[] picture { get; set; }

        public UserPictureModel(Customer c)
        {
            this.picture = c.Picture;
        }
    }
}
