﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServisCepte.Models.PocoModels
{
    public class SubLastStatusModel
    {
        public int Id { get; set; }
        public string caption { get; set; }
        public sbyte? text_in { get; set; }
        public sbyte? numeric_in { get; set; }
        public sbyte? integer_in { get; set; }
        public sbyte? deletion { get; set; }
        public int? services_last_status_defination_id { get; set; }
        public sbyte? personel_in { get; set; }
        public sbyte? date_in { get; set; }
        public sbyte? force_entery { get; set; }
        public sbyte? order_number { get; set; }
        public int? control_type { get; set; }
        

        public SubLastStatusModel(ServicesLastStatusDefinationInfs slsd)
        {
            this.Id = slsd.Id;
            this.caption = slsd.Caption;
            this.text_in = slsd.TextIn;
            this.numeric_in = slsd.NumericIn;
            this.integer_in = slsd.IntegerIn;
            this.deletion = slsd.Deletion;
            this.services_last_status_defination_id = slsd.ServicesLastStatusDefinationId;
            this.personel_in = slsd.PersonelIn;
            this.date_in = slsd.DateIn;
            this.force_entery = slsd.ForceEntery;
            this.order_number = slsd.OrderNumber;
        }
    }

    
}
