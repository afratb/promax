﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ProducerReceiptExcelImportTemp
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public string AccountTitle { get; set; }
        public decimal? Litre { get; set; }
        public int? Basket { get; set; }

        public Customer Customer { get; set; }
    }
}
