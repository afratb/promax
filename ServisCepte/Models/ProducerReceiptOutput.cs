﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ProducerReceiptOutput
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public int? ProductId { get; set; }
        public int? StorageId { get; set; }
        public string DriverName { get; set; }
        public string Plate { get; set; }
        public string CustomerTitle { get; set; }
        public decimal? Qty { get; set; }
        public DateTime? DateTime { get; set; }
        public DateTime? InsDatetime { get; set; }
        public int? Deletion { get; set; }

        public Customer Customer { get; set; }
        public Product Product { get; set; }
        public Storage Storage { get; set; }
    }
}
