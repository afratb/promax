﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class RestaurantOrderTemp
    {
        public RestaurantOrderTemp()
        {
            RestaurantOrderListTemp = new HashSet<RestaurantOrderListTemp>();
        }

        public int Id { get; set; }
        public int? TableId { get; set; }
        public int? Basket { get; set; }
        public string CountOfCustomer { get; set; }
        public int? TableNumber { get; set; }
        public DateTime? InsDate { get; set; }
        public sbyte? BillCollection { get; set; }
        public int? UserId { get; set; }
        public int? BillUserId { get; set; }

        public ICollection<RestaurantOrderListTemp> RestaurantOrderListTemp { get; set; }
    }
}
