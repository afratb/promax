﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class RestaurantTables
    {
        public int Id { get; set; }
        public string TableCode { get; set; }
        public int? DepartmanId { get; set; }
        public int? TableNumber { get; set; }
        public string TableExample { get; set; }
        public string TableCapacity { get; set; }
    }
}
