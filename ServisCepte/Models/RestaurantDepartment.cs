﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class RestaurantDepartment
    {
        public int Id { get; set; }
        public string DepartmentName { get; set; }
    }
}
