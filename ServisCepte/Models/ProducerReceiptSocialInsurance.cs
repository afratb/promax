﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ProducerReceiptSocialInsurance
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? Date1 { get; set; }
        public DateTime? Date2 { get; set; }
        public decimal? Currency { get; set; }
        public sbyte? Stats { get; set; }
        public int? DetailId { get; set; }

        public Customer Customer { get; set; }
    }
}
