﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class SpeedSalesTabsheetCaptions
    {
        public int Id { get; set; }
        public string Tabsheetname { get; set; }
        public string Tabsheetcaption { get; set; }
        public int? UserId { get; set; }
    }
}
