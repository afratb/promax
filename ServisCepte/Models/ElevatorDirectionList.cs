﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorDirectionList
    {
        public int Id { get; set; }
        public string DirectionName { get; set; }
    }
}
