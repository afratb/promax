﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class RestaurantPrinterParameters
    {
        public int Id { get; set; }
        public int? ProductGroup3Id { get; set; }
        public string ProductGroup3Name { get; set; }
        public string PrinterName { get; set; }
    }
}
