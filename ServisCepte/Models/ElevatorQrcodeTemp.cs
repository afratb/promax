﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorQrcodeTemp
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public sbyte? ControlType { get; set; }
        public string ControlDefine { get; set; }
        public string Qrcode { get; set; }
        public int? Basket { get; set; }
        public int? ElevatorOrder { get; set; }
    }
}
