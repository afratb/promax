﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorOfferGroupsTemp
    {
        public int Id { get; set; }
        public int? OrderId { get; set; }
        public int? ElevatorOfferTempId { get; set; }
        public int? ElevatorCount { get; set; }
        public int? FloorCount { get; set; }
        public int? StationCount { get; set; }
        public int? RunDistance { get; set; }
        public int? PersonCapacity { get; set; }
        public int? KgCapacity { get; set; }
        public decimal? OfferCurrency { get; set; }
    }
}
