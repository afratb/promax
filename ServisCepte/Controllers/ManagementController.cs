﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServisCepte.Attributes;
using ServisCepte.Models;
using ServisCepte.Models.PocoModels;

namespace ServisCepte.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class ManagementController : ControllerBase
    {
        public crocodileContext _context;
        public List<ManagementModel> managementModel = new List<ManagementModel>();
        public List<UserPictureModel> UserPictureList = new List<UserPictureModel>();

        public ManagementController(crocodileContext context)
        {
            _context = context;
        }

        [BasicAuthorize("serviscepte")]
        [HttpGet]
        [Route("login_check")]
        public JsonResult Login()
        {
            var id = HttpContext.User.Identities.Last().Claims.First().Value;
            managementModel.Add(new ManagementModel(Convert.ToInt32(id)));
            return new JsonResult(managementModel.ToList());
        }

        [HttpGet]
        [Route("user_picture")]
        public JsonResult UserPic()
        {
            var list = _context.Customer.ToList();
            list.ForEach(k => UserPictureList.Add(new UserPictureModel(k)));
            return new JsonResult(UserPictureList.ToList());
        }
    }
}