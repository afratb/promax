﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServisCepte.Models;
using ServisCepte.Models.PocoModels;

namespace ServisCepte.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        public crocodileContext _context;
        public List<CompanyLogoModel> CompanyLogoList = new List<CompanyLogoModel>();
        public List<CustomerModel> CustomerList = new List<CustomerModel>();
        public List<AccountActivitiesModel> AccountActivitiesList = new List<AccountActivitiesModel>();

        public CustomerController(crocodileContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("company_logo")]
        public JsonResult CompanyLogo()
        {
            var list = _context.GeneralParameters.ToList();
            list.ForEach(k => CompanyLogoList.Add(new CompanyLogoModel(k)));
            return new JsonResult(CompanyLogoList.ToList());
        }

        [HttpGet]
        [Route("customer_list")]
        public JsonResult Customer()
        {
            var list = _context.Customer.ToList();
            list.ForEach(k => CustomerList.Add(new CustomerModel(k)));
            return new JsonResult(CustomerList.ToList());
        }

        [HttpGet]
        [Route("account_activities")]
        public JsonResult Account()
        {
            var list = _context.CustomerCurrentInput.ToList();
            list.ForEach(k => AccountActivitiesList.Add(new AccountActivitiesModel(k)));
            return new JsonResult(AccountActivitiesList.ToList());
        }
    }
}