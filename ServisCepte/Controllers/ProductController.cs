﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServisCepte.Models;
using ServisCepte.Models.PocoModels;

namespace ServisCepte.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        public crocodileContext _context;
        public List<ProductModel> ProductList = new List<ProductModel>();

        public ProductController(crocodileContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("product_list")]
        public JsonResult Procuct()
        {ProductList = new List<ProductModel>();
            var list = _context.Product.ToList();
            list.ForEach(k => ProductList.Add(new ProductModel(k)));
            return new JsonResult(ProductList.ToList());
        }
    }
}