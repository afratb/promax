﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServisCepte.Models;
using ServisCepte.Models.PocoModels;

namespace ServisCepte.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class ServicesController : ControllerBase
    {
        public crocodileContext _context;
        public List<ServiceRequestModel> Serviceslist = new List<ServiceRequestModel>();
        public List<SubControlDefinesModel> SubControlDefinesList = new List<SubControlDefinesModel>();
        public List<ModuleDefinesModel> ModuleDefinesList = new List<ModuleDefinesModel>();
        public List<ControlDefinesModel> ControlDefinesList = new List<ControlDefinesModel>();
        public List<LastStatusModel> LastStatusList = new List<LastStatusModel>();
        public List<SubLastStatusModel> SubLastStatusList = new List<SubLastStatusModel>();
        public List<ServicePointModel> ServicePointList = new List<ServicePointModel>();

        public ServicesController(crocodileContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("service_request_list")]
        public JsonResult ServiceList()
        {
            var list = _context.ServicesRequestDefination.ToList();
            list.ForEach(k => Serviceslist.Add(new ServiceRequestModel(k)));
            return new JsonResult(Serviceslist.ToList());
        }

        [HttpGet]
        [Route("service_sub_control_defines")]
        public JsonResult SubControlDefines()
        {
            var list = _context.ServiceControlSubDefines.ToList();
            list.ForEach(k => SubControlDefinesList.Add(new SubControlDefinesModel(k)));
            return new JsonResult(SubControlDefinesList.ToList());
        }

        [HttpGet]
        [Route("service_module_defines")]
        public JsonResult ModuleDefines()
        {
            var list = _context.ServicesCategorySubDefinition.ToList();
            list.ForEach(k => ModuleDefinesList.Add(new ModuleDefinesModel(k)));
            return new JsonResult(ModuleDefinesList.ToList());
        }

        [HttpGet]
        [Route("service_control_defines")]
        public JsonResult ControlDefines()
        {
            var list = _context.ServiceControlDefines.ToList();
            list.ForEach(k => ControlDefinesList.Add(new ControlDefinesModel(k)));
            return new JsonResult(ControlDefinesList.ToList());
        }

        [HttpGet]
        [Route("service_last_status_list")]
        public JsonResult LastStatus()
        {
            var list = _context.ServicesLastStatusDefination.ToList();
            list.ForEach(k => LastStatusList.Add(new LastStatusModel(k)));
            return new JsonResult(LastStatusList.ToList());
        }

        [HttpGet]
        [Route("service_last_status_sub_list")]
        public JsonResult SubLastStatus()
        {
            var list = _context.ServicesLastStatusDefinationInfs.ToList();
            list.ForEach(k => SubLastStatusList.Add(new SubLastStatusModel(k)));
            return new JsonResult(SubLastStatusList.ToList());
        }

        [HttpGet]
        [Route("service_point_list")]
        public JsonResult ServicePoint()
        {
            var list = _context.ServicesPointsBaseInformation.ToList();
            Customer c = _context.Customer.Where(i => i.Id == 1).SingleOrDefault();
            ServicesContract sc = _context.ServicesContract.Where(i => i.Id == 1).SingleOrDefault();
            ServicesCategoryDefinition scd = _context.ServicesCategoryDefinition.Where(i => i.Id == 1).SingleOrDefault();
            list.ForEach(k => ServicePointList.Add(new ServicePointModel(k, c, sc, scd)));
            return new JsonResult(ServicePointList.ToList());
        }
    }
}